﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SD.Common;

namespace SocketClient
{
    public partial class frmWrapperManager : Form
    {
        public frmWrapperManager()
        {
            InitializeComponent();

            Fill();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmWrapperManager_Load(object sender, EventArgs e)
        {

        }

        private WrapperManager WrapperManager
        {
            get
            {
                return App.Default.WrapperManager;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void Fill()
        {
            this.lvWrapper.Items.Clear();

            foreach (IWrapper w in WrapperManager.WrapperCollection)
            {
                ListViewItem lvi = CreateLvi(w);
                this.lvWrapper.Items.Add(lvi);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="w"></param>
        /// <returns></returns>
        private ListViewItem CreateLvi(IWrapper w)
        {
            ListViewItem lvi = new ListViewItem(w.GetType().Name);
            lvi.SubItems.AddRange(new string[] { w.Description });
            lvi.Checked = w.Enabled;
            lvi.Tag = w;
            return lvi;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvWrapper_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            IWrapper w = e.Item.Tag as IWrapper;
            w.Enabled = e.Item.Checked;

            string fullName = w.GetType().FullName;
            if (e.Item.Checked)
            {
                Config.EnabledWrapperList.Add(fullName);
            }
            else
            {
                Config.EnabledWrapperList.Remove(fullName);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private Config Config
        {
            get { return App.Default.Config; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
